package br.com.webtask.aula.controllers;

import br.com.webtask.aula.WebTaskApplication;
import br.com.webtask.aula.config.security.user.UserDetailsServiceImpl;
import br.com.webtask.aula.controller.UserController;
import br.com.webtask.aula.controller.service.TaskService;
import br.com.webtask.aula.controller.service.UserService;
import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.util.LogadoUtil;
import br.com.webtask.aula.utils.TestUtils;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = WebTaskApplication.class)
public class UserControllerTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "cliente.json";

    @Autowired
    private MockMvc mock;

    @MockBean
    private LogadoUtil userLogado;

    @MockBean
    private UserRepo userRepo;

    @MockBean
    private UserService userService;

    @MockBean
    private TaskRepo taskRepo;

    @MockBean
    private TaskService taskService;

    @MockBean
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    private RestTemplate restTemplate;

    protected static UserClient getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, UserClient.class);
    }

    //FIXME: testes não funcionaram devido a autenticação.

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_CLIENTE"})
    public void deveAcessarEditarusuario() throws Exception {

        UserClient uc1 = UserClient.builder()
                .ativo(true)
                .cpf("123")
                .email("admin@admin")
                .name("admin")
                .senha(new BCryptPasswordEncoder().encode("123"))
                .endereco("Rua A, 123")
                .telefone("32991042583")
                .build();

        when(userService.buscarUsuario(anyLong())).thenReturn(uc1);
        ResultActions ra = mock.perform(get("/user/editar"));
        ra.andExpect( status().isOk() ).andDo(print());
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_ADMIN"})
    public void deveEditarUsuario() throws Exception {

        ResultActions ra = mock.perform(post("/user/editar")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).with(csrf())
                .param("name", "trocado")
                .param("email", "trocado@trocado.com")
                .param("senha", "senhaTrocada")
        );

        ra.andExpect( status().isFound() )
                .andDo(print());
    }
}
