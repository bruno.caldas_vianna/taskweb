package br.com.webtask.aula.controllers;

import br.com.webtask.aula.WebTaskApplication;
import br.com.webtask.aula.config.security.user.UserDetailsServiceImpl;
import br.com.webtask.aula.controller.TaskController;
import br.com.webtask.aula.controller.service.TaskService;
import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.util.LogadoUtil;
import br.com.webtask.aula.utils.TestUtils;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = WebTaskApplication.class)
public class TaskControllerTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "tarefas.json";

    @Autowired
    private MockMvc mock;

    @MockBean
    private LogadoUtil userLogado;

    @MockBean
    private UserRepo userRepo;

    @MockBean
    private TaskRepo taskRepo;

    @MockBean
    private TaskService taskService;

    @MockBean
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    private RestTemplate restTemplate;

    protected static Task getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, Task.class);
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_CLIENTE"})
    public void deveAcessarNovaTask() throws Exception {

        ResultActions ra = mock.perform(get("/task"));
        ra.andExpect( status().isOk() ).andDo(print());
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_CLIENTE"})
    public void deveListarAsTasks() throws Exception {

        List<Task> taskList = new ArrayList<>();
        taskList.add(getMockObject());
        when(taskService.minhaLista(any(Long.class))).thenReturn(taskList);

        ResultActions ra = mock.perform(get("/task/list"));

        ra.andExpect( status().isOk() )
                .andDo(print());
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_ADMIN"})
    public void deveSalvarNovaTask() throws Exception {

        ResultActions ra = mock.perform(post("/task")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED).with(csrf())
                .param("plannedDate", "2020-09-24")
                .param("taskDescription", "Teste")
        );

        ra.andExpect( status().isFound() )
                .andDo(print());
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_ADMIN"})
    public void deveFecharATask() throws Exception {

        when(taskService.getTask(1L)).thenReturn(getMockObject());
        ResultActions ra = mock.perform(get("/task/1/close")
        );

        ra.andExpect( status().isFound() )
                .andDo(print());
    }
}
