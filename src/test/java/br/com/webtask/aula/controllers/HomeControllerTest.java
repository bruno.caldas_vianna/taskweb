package br.com.webtask.aula.controllers;

import br.com.webtask.aula.WebTaskApplication;
import br.com.webtask.aula.config.security.user.UserDetailsServiceImpl;
import br.com.webtask.aula.config.security.user.UserLogado;
import br.com.webtask.aula.controller.HomeController;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.util.LogadoUtil;
import br.com.webtask.aula.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(HomeController.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = WebTaskApplication.class)
public class HomeControllerTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "cliente.json";


    @Autowired
    private MockMvc mock;

    @MockBean
    private LogadoUtil userLogado;

    @MockBean
    private UserRepo userRepo;

    @MockBean
    private TaskRepo taskRepo;

    @MockBean
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private PasswordEncoder password;

    private UserClient userClient = null;

    protected static UserClient getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, UserClient.class);
    }

   @Before
    public void adicionarUsuario() {

       userClient = UserClient.builder()
               .ativo(true)
               .cpf("123")
               .email("admin@admin")
               .name("admin")
               .senha(password.encode("123"))
               .endereco("Rua A, 123")
               .telefone("32991042583")
               .build();
       userRepo.save(userClient);
   }

    @Test
    public void deveRedirecionarParaLogin() throws Exception {

        ResultActions ra = mock.perform(get("/home"));

        ra.andExpect( status().isFound() )
                .andExpect(redirectedUrl("http://localhost/login"))
                .andDo(print());
    }

    @Test
    public void deveAcessarLogin() throws Exception {

        ResultActions ra = mock.perform(get("/login"));

        ra.andExpect( status().isOk() )
                .andDo(print());
    }

    @Test
    public void deveNaoAutorizarComLoginErrado() throws Exception {

                mock.perform(post("/login")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("username", "123")
                        .param("password", "ff")
                )
                        .andExpect(status().is4xxClientError());
    }

    @Test
    public void deveAutoizarAcessoDeAdmin() throws Exception {

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        when(userDetailsServiceImpl.loadUserByUsername("123")).thenReturn( new UserLogado("123", "123", grantedAuthorities,1, "admin"
                ,"admin@admin.com.br"));

        RequestBuilder requestBuilder = post("/login", formLogin().user("123").password("123")).with(csrf());
        mock.perform(requestBuilder)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(cookie().exists("JSESSIONID"));
    }

    @Test
    @WithMockUser(username = "123", password = "123", authorities = {"ROLE_CLIENTE"})
    public void deveAcessarAHome() throws Exception {

        when(userLogado.getNomeUserLogado(any())).thenReturn("Cliente");
        ResultActions ra = mock.perform(get("/home"));
        ra.andExpect( status().isOk() ).andDo(print());
    }
}
