package br.com.webtask.aula.services;

import br.com.webtask.aula.controller.service.TaskService;
import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TasksServiceTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "tarefas.json";

    @MockBean
    private TaskRepo repository;

    private TaskService taskService(){
        return new TaskService(repository);
    }

    protected Task getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, Task.class);
    }

    /**
     * O sistema deve permitir ao usuário listar as tarefas referentes a ele.
     */
    @Test
    public void testarListarTaskDeUsuario(){
        UserClient user = ClientServiceTest.getMockObject();
        Task task = getMockObject();
        //injetar o cliente na task
        task.setUser(ClientServiceTest.getMockObject());
        when(repository.findByUserIdOrderByPlannedDateDesc(user.getId())).thenReturn(Collections.singletonList(task));
        List<Task> minhaLista = taskService().minhaLista(user.getId());
        Assert.assertEquals(1, minhaLista.size());
    }

    /**
     * Testar se o sistema vai lançar exceção ao tentar salvar uma tarefa nula
     */
    @Test(expected = NullPointerException.class)
    public void testarLancarExcecaoAoSalvarTaskNula(){
        Task task = taskService().salvar(null);
        System.out.println(task.getTaskDescription());
    }

    /**
     * O sistema deve permitir o fechamento de uma tarefa
     */
    @Test
    public void testarFinalizarTask(){
        Task task = getMockObject();
        task.setFinishedDate(LocalDate.now());
        when(repository.save(task)).thenReturn(task);
        Task taskFinalizada = taskService().finalizar(task);
        Assert.assertEquals(LocalDate.now(), task.getFinishedDate());
    }

    /**
     * O sistema deve permitir que uma tarefa seja buscada pelo id
     */
    @Test
    public void testarBuscarTarefaPeloId(){
        Task task = getMockObject();
        when(repository.getOne(task.getId())).thenReturn(task);
        Task taskFinalizada = taskService().getTask(task.getId());
        Assert.assertEquals(task.getTaskDescription(), taskFinalizada.getTaskDescription());
        Assert.assertEquals(task.getPlannedDate(), taskFinalizada.getPlannedDate());
    }
}
