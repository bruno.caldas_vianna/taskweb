package br.com.webtask.aula.services;

import br.com.webtask.aula.controller.service.UserService;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.utils.TestUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ClientServiceTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "cliente.json";

    @MockBean
    private UserRepo repository;

    private UserService userService(){
        return new UserService(repository);
    }

    protected static UserClient getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, UserClient.class);
    }

    /**
     * O sistema só deve permitir que o cliente se cadastre caso os campos nome, cpf, endereço, telefone e email estejam preenchidos.
     */
    @Test
    public void testarCamposObrigatoriosAoCadastrar(){
        UserClient client = getMockObject();
        when(repository.save(client)).thenReturn(client);
        UserClient userClient = userService().salvar(client);
        Assert.assertNotNull(userClient.getName());
        Assert.assertNotNull(userClient.getCpf());
        Assert.assertNotNull(userClient.getEmail());
        Assert.assertNotNull(userClient.getEndereco());
        Assert.assertNotNull(userClient.getTelefone());
    }

    @Test
    public void testarSalvarCliente(){
        UserClient client = getMockObject();
        when(repository.save(any(UserClient.class))).thenAnswer(i -> i.getArguments()[0]);
        UserClient userClient = userService().salvar(client);
        Assert.assertNotNull(userClient);
    }

    /**
     * Testando se o sistema consegue buscar um usuário pelo id
     */
    @Test
    public void testarBuscarUsuarioPeloId(){
        UserClient client = getMockObject();
        when(repository.getOne(client.getId())).thenReturn(client);
        UserClient usuarioBuscado = userService().buscarUsuario(client.getId());
        Assert.assertNotNull(usuarioBuscado);
    }

    /**
     * Testando se o sistema ira lançar exceção caso nao encontre o id passado
     */
    @Test(expected = NullPointerException.class)
    public void testarLancarExececaoIdInexistente(){
        long id = new Random().nextLong();
        when(repository.getOne(id)).thenReturn(null);
        UserClient usuarioBuscado = userService().buscarUsuario(id);
        //TODO: Ao tentar acessar qualquer atributo do usuário irá lancar um null pointer pois o mock retorna null
        System.out.println(usuarioBuscado.getName());
    }
}
