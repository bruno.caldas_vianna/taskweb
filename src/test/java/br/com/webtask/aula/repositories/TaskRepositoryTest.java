package br.com.webtask.aula.repositories;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.TaskRepo;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.utils.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class TaskRepositoryTest {

    @Autowired
    UserRepo userRepo;

    @Autowired
    TaskRepo taskRepo;

    private UserClient u1, u2, u3;

    private Task t1, t2, t3, t4;

    @BeforeEach
    public void init() {
        u1 = new UserClient(1L, "Bruno", "123456", "bruno@vianna.com.br", "Rua A, 123", "99999999", "123", true, null);
        u2 = new UserClient(2L, "Zezinho", "684651", "zezinho@vianna.com.br", "Rua C, 500", "77777777", "123", true, null);
        u3 = new UserClient(3L, "Ana", "654321", "ana@viannajr.com.br", "Rua B, 345", "88888888", "123", true, null);

        u1 = userRepo.save(u1);
        u2 = userRepo.save(u2);
        u3 = userRepo.save(u3);

        t1 = new Task(1L, "Tirar o Lixo", LocalDate.now().plusDays(2), LocalDate.now().plusDays(3), u3);
        t2 = new Task(2L, "Fazer Compras", LocalDate.now(), LocalDate.now().plusDays(1), u1);
        t3 = new Task(3L, "Lavar Roupa", LocalDate.now().plusDays(2), LocalDate.now().plusDays(3), u2);
        t4 = new Task(4L, "Fazer Almoço", LocalDate.now().plusDays(1), LocalDate.now().plusDays(2), u1);

        List<Task> tasks = new ArrayList<>();
        tasks.add(t1);
        tasks.add(t2);
        tasks.add(t3);
        tasks.add(t4);
        taskRepo.saveAll(tasks);
    }

    @AfterEach
    public void clean() {
        taskRepo.deleteAll();
        userRepo.deleteAll();
    }

    @Test
    public void verificaQuantidadeDeTarefas() {
        long qtdTasks = taskRepo.count();
        Assertions.assertThat(qtdTasks).isEqualTo(4);
    }

    @Test
    public void testarSalvarNovaTask() {
        Task novaTask = new Task("Lavar Roupa", LocalDate.now().plusDays(2), LocalDate.now().plusDays(3), u2);
        Task taskSalva = taskRepo.save(novaTask);
        Assertions.assertThat(taskSalva.getId()).isPositive();
    }

    @Test
    public void testarBuscarPorDescricao() {
        List<Task> tasks = taskRepo.findByTaskDescription("Tirar o Lixo");
        Assertions.assertThat(tasks).isNotEmpty();
    }

    @Test
    public void testarFalhaBuscarPorDescricao() {
        List<Task> tasks = taskRepo.findByTaskDescription("Tarefa inexistente");
        Assertions.assertThat(tasks).isEmpty();
    }

    @Test
    public void testarBuscarTarefaPorUsuario() {
        List<Task> tasks = taskRepo.findByUserId(2L);
        Assertions.assertThat(tasks).isNotEmpty();
    }

    @Test
    public void testarBuscarTarefaPorUsuarioOrderNadoPorPlannedDateDesc() {
        List<Task> tasks = taskRepo.findByUserIdOrderByPlannedDateDesc(1L);
        Assertions.assertThat(tasks.get(0).getId()).isEqualTo(4L);
    }

}
