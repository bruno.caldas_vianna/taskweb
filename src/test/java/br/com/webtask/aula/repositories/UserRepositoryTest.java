package br.com.webtask.aula.repositories;

import br.com.webtask.aula.domain.model.UserClient;
import br.com.webtask.aula.domain.repo.UserRepo;
import br.com.webtask.aula.utils.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    UserRepo userRepo;

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "cliente.json";

    private UserClient u1, u2, u3;

    protected static UserClient getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, UserClient.class);
    }

    @BeforeEach
    public void init() {
        u1 = new UserClient(0L, "Bruno", "123456", "bruno@vianna.com.br", "Rua A, 123", "99999999", "123", true, null);
        u2 = getMockObject();
        u3 = new UserClient(1L, "Ana", "654321", "ana@viannajr.com.br", "Rua B, 345", "88888888", "123", true, null);

        userRepo.save(u1);
        userRepo.save(u2);
        userRepo.save(u3);
    }

    @AfterEach
    public void clean() {
        userRepo.deleteAll();
    }

    @Test
    public void verificaQuantidadeDeusuarios() {
        long qtdusuario = userRepo.count();
        Assertions.assertThat(qtdusuario).isEqualTo(3);
    }

    @Test
    public void testarSalvarNovoCliente() {
        UserClient user = new UserClient("Miroldo", "102548", "miroldo@vianna.com.br", "Rua M, 123", "88888888", "123", true, null);
        UserClient userSalvo = userRepo.save(user);
        Assertions.assertThat(userSalvo.getId()).isPositive();
    }

    @Test
    public void testarBuscarPorNome() {
        UserClient userEncontrado = userRepo.findByName("Bruno").get();
        Assertions.assertThat(userEncontrado.getName()).isEqualTo("Bruno");
    }

    @Test
    public void testarBuscarPorCpf() {
        UserClient userEncontrado = userRepo.findByCpf("123456").get();
        Assertions.assertThat(userEncontrado.getName()).isEqualTo("Bruno");
    }

}
