/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.models;

import br.com.webtask.aula.domain.model.EStatus;
import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.utils.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author daves
 */
public class TaskTest {

    public static final String MOCK_FOLDER = "/mocks";
    public static final String JSON = "tarefas.json";

    protected Task getMockObject(){
        return TestUtils.getMockObject(MOCK_FOLDER, JSON, Task.class);
    }
    
    @Test
    public void testTaskFinalizada() {
        Task task = getMockObject();
         Assertions.assertThat(task.isFinish()).isTrue();
    }

    @Test
    public void testTaskNaoFinalizada() {
        Task task = getMockObject();
        task.setFinishedDate(null);
        Assertions.assertThat(task.isFinish()).isFalse();
    }

    @Test
    public void testTaskAtrasada() {
        Task task = getMockObject();
        task.setPlannedDate(LocalDate.now().minusDays(1));
        Assertions.assertThat(task.isLate()).isTrue();
    }

    @Test
    public void testTaskNaoAtrasada() {
        Task task = getMockObject();
        task.setPlannedDate(LocalDate.now().plusDays(1));
        Assertions.assertThat(task.isLate()).isFalse();
    }

    @Test
    public void testTaskStatusNovo() {
        Task task = getMockObject();
        task.setFinishedDate(null);
        task.setPlannedDate(LocalDate.now());
        Assertions.assertThat(task.getStatus()).isEqualByComparingTo(EStatus.NOVO);
    }

    @Test
    public void testTaskStatusAtrasado() {
        Task task = getMockObject();
        task.setPlannedDate(LocalDate.now().minusDays(3));
        task.setFinishedDate(null);
        Assertions.assertThat(task.getStatus()).isEqualByComparingTo(EStatus.ATRASADO);
    }

    @Test
    public void testTaskStatusConcluidoPrazo() {
        Task task = getMockObject();
        task.setPlannedDate(LocalDate.now());
        task.setFinishedDate(LocalDate.now().minusDays(1));
        Assertions.assertThat(task.getStatus()).isEqualByComparingTo(EStatus.CONCLUIDO_PRAZO);
    }

    @Test
    public void testTaskStatusConcluidoAtrasado() {
        Task task = getMockObject();
        task.setPlannedDate(LocalDate.now());
        task.setFinishedDate(LocalDate.now().plusDays(1));
        Assertions.assertThat(task.getStatus()).isEqualByComparingTo(EStatus.CONCLUIDO_ATRASADO);
    }

}
