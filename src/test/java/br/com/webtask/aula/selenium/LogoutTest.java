package br.com.webtask.aula.selenium;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;

public class LogoutTest {

    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();

        driver.get("http://localhost:8080/login");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("123");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void logout() {
        driver.get("http://localhost:8080/");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.linkText("Sair")).click();

        Assertions.assertThat(driver.getCurrentUrl()).isEqualTo("http://localhost:8080/home");
    }
}

