package br.com.webtask.aula.selenium;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;

public class LoginTest {
    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;
    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
//        options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application");
        driver = new ChromeDriver(options);
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }
    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testLoginCorreto() {
        driver.get("http://localhost:8080/login");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("123");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();
        Assertions.assertThat(driver.findElement(By.tagName("nav")).isDisplayed() );
    }

    @Test
    public void testLoginInCorreto() {
        driver.get("http://localhost:8080/login");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("000");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("000");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();

        Assertions.assertThat(driver.findElement(By.cssSelector(".text-danger")).getText() ).isEqualTo("Login ou Senha incorreta");
    }

    @Test
    public void testLoginCampoVazio() {
        driver.get("http://localhost:8080/login");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("000");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.cssSelector(".login100-form-btn")).click();

        Assertions.assertThat(driver.findElement(By.cssSelector(".alert-validate")).isDisplayed());
    }
}
