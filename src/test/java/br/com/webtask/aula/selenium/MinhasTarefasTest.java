package br.com.webtask.aula.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinhasTarefasTest {

    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();

        driver.get("http://localhost:8080/login");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).sendKeys("123");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.cssSelector(".login100-form-btn")).click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void minhasTarefas() {
        driver.get("http://localhost:8080/");
        driver.manage().window().setSize(new Dimension(1920, 1030));
        driver.findElement(By.linkText("Minhas Tarefas")).click();
        {
            List<WebElement> elements = driver.findElements(By.id("listagem"));
            assert(elements.size() > 0);
        }
    }
}
