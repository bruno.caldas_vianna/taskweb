package br.com.webtask.aula.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.io.InputStream;

public final class TestUtils {
    private final static ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    public static <T> T getMockObject(String mockFolder, String fileName, Class<T> targetClazz) {
        String filePath = mockFolder + "/" + fileName;
        try (InputStream is = TestUtils.class.getResourceAsStream(filePath)) {
            return mapper.readValue(is, targetClazz);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Um erro ocorreu ao carregar o JSON de teste: " + filePath);
        }
    }
}
